<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class OptionType extends Model
{
    use Rememberable;
    protected $fillable = [
        'option_cat_id',
        'name',
        'slug'
    ];

    protected $table = 'options_types';

    public static $featuredGeneralOptions = [
        "Adaptive Cruise Control",
        "Alloy Wheels",
        "Apple CarPlay/Android Auto",
        "Automatic Emergency Braking",
        "Backup Camera",
        "Blind Spot Monitor",
        "Bluetooth",
        "Brake Assist",
        "Cooled Seats",
        "Heated Seats",
        "Heated Steering Wheel",
        "HomeLink",
        "Keyless Start",
        "LED Headlights",
        "Lane Departure Warning",
        "Leather Seats",
        "Memory Seat",
        "Navigation System",
        "Premium Sound System",
        "Rear Seat Entertainment",
        "Remote Start",
        "Stability Control",
        "Sunroof/Moonroof",
        "Third Row Seating",
        "Tow Hitch"
    ];

    public function category()
    {
        return $this->hasOne(OptionTypeCategory::class,'id');
    }

}
