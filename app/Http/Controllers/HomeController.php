<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\OptionType;
use Carbon\Carbon;
use App\Dealer;
use DB;
use App\ProductOption;
use Http\Client\Curl\Client;
use Mail;
use App\Mail\ContactMessageSent;
use Validator;

class HomeController extends Controller
{

    public $pagination = 10;

    public function getAllMakes()
    {
        $products = Product::where('make', '!=', '')->remember(1440)->pluck('make')->unique();

        return response()->json($products);
    }

    public function getAllModels()
    {
        $products = Product::where('model', '!=', '')->remember(1440)->pluck('model')->unique();

        return response()->json($products);
    }

    public function getMakesByType($type = '')
    {
        if ($type == 'all') {
            $products = Product::where('make', '!=', '')->remember(1440)->pluck('make')->unique();
        } else {
            $products = Product::where('type', $type)->where('make', '!=', '')->remember(1440)->pluck('make')->unique();
            // $products = Product::searchByQuery(array('match' => array('type' => $type)))->where('make', '!=', '')->pluck('make')->unique();
        }

        return response()->json($products);
    }

    public function getModelsByMake($make = '')
    {
        if ($make == '') {
            $products = Product::where('model', '!=', '')->remember(1440)->pluck('model')->unique();
        } else {
            $products = Product::where('make', $make)->remember(1440)->pluck('model')->unique();
        }

        return response()->json($products);
    }

    public function getAllYears()
    {
        $products = Product::where('year', '>', 1900)->selectRaw('MIN(year) as min, MAX(year) as max')->get()->toArray();

        return response()->json($products);
    }

    public function getAllPrices()
    {
        $max_price = Product::max('sellingprice');

        $prices = array();
        $price = 0;
        for ($i = 1; $i < 50; $i++) {
            if ($price < 10000) {
                $price = 2000 + $price;
                array_push($prices, $price);
            } elseif ($price >= 10000  && $price < 50000) {
                $price = 5000 + $price;
                array_push($prices, $price);
            } elseif ($price >= 50000  && $price < 100000) {
                $price = 10000 + $price;
                array_push($prices, $price);
            } elseif ($price >= 100000  && $price < 200000) {
                $price = 25000 + $price;
                array_push($prices, $price);
            } elseif ($price >= 200000  && $price < 500000) {
                $price = 50000 + $price;
                array_push($prices, $price);
            }
        }

        return response()->json($prices);
    }

    public function getAllMileages()
    {
        $max_mileage = Product::max('miles');

        $mileages = [20, 30, 40, 50, 75, 100, 150, 200, 250, 500];

        return response()->json($mileages);
    }

    public function getAllBodyStyles()
    {
        $products = Product::selectRaw('body, count(body) as count')->where('body', '!=', '')->groupBy('body')->remember(1440)->get();

        return response()->json($products);
    }

    public function getAllSuvProducts()
    {
        $products = Product::with(['images'])->has('images')->where('body', 'SUV')->limit(10)->remember(1440)->get();

        return response()->json($products);
    }

    public function getAllSedanProducts()
    {
        $products = Product::with(['images'])->has('images')->where('body', 'Sedan')->limit(10)->remember(1440)->get();

        return response()->json($products);
    }

    public function getAllTruckProducts()
    {
        $products = Product::with(['images'])->has('images')->where('body', 'Pickup')->limit(10)->remember(1440)->get();

        return response()->json($products);
    }

    public function getAllPopularProducts()
    {
        $latitude = 43.1856307;
        $longitude = -77.7565881;
        $distance = 100;

        //get recent product new product
        $productrecent = Product::selectRaw(DB::raw('*, round(st_distance_sphere(geo, Point('.$longitude .', '.$latitude .'))*0.000621371192,3)  as distance'))
                                ->whereRaw('st_distance_sphere(geo, Point(?, ?))*0.000621371192  < ?', [$longitude , $latitude, $distance])
                                ->where('type','New')
                                ->orderBy('distance', 'ASC')
                                ->limit(15)
                                ->remember(1440)
                                ->get();

        //get used product
        $usedcars = Product::where('type', 'Used')->limit(15)->remember(1440)->get();

        //get certified cars
        $certified_greatcars = Product::where('certified', 'True')->limit(15)->remember(1440)->get();

        $data['certified_greatcars'] = $certified_greatcars;
        $data['recentproduct'] = $productrecent;
        $data['usedcar'] = $usedcars;

        return response()->json($data);
    }

    /*
	* Search Cars Listing
	*/

    public function SearchProducts(Request $request)
    {
        $filterresult = $this->filterRequest($request);
        $search_filterable = [];

        if ($filterresult) {
            if (!empty($request->search_zip)) {
                $search_filterable['product'] = $filterresult->inRandomOrder()->limit(2000)->get();
            } else {
                $filterresult = $filterresult->with(['images', 'dealer'])->inRandomOrder()->limit(2000)->get();
                if ($request->photos == 'yes') {
                    $filterresult = $filterresult->with(['images', 'dealer'])->has('images')->inRandomOrder()->limit(2000)->get();
                }
                $search_filterable['product'] = $filterresult;
            }
        } else {
            $products = Product::with(['images'])->with(['dealer'])->where('type', $request->type)->inRandomOrder()->limit(2000)->get();
            if ($request->type == 'all') {
                $products = Product::with(['images'])->with(['dealer'])->inRandomOrder()->limit(2000)->get();
            }
            $search_filterable['product'] = $products;
        }

        $product_option = OptionType::$featuredGeneralOptions;

        $search_filterable['product_options'] = $product_option;

        return response()->json($search_filterable);
    }

    /*
	* Filter Functionality
	*/

    private function filterRequest($request, $for = '')
    {
        $querystring = $request->all();

        if (array_key_exists('search_make', $querystring) && array_key_exists('search_model', $querystring) && array_key_exists('search_body', $querystring)) {
            unset($querystring['search_body']);
        }

        if (empty($querystring)) {
            return [];
        }

        $type = [
            'market_days' => 'val',
            'type' => 'type',
            'exteriorcolor' => 'option',
            'interiorcolor' => 'option',
            'drivetrain' => 'option',
            'enginecylinders' => 'option',
            'transmission' => 'option',
            'transmission_speed' => 'option',
            'engine' => 'multiple option',
            'fuel_type' => 'option',
            'body' => 'option',
            'make' => 'option',
            'model' => 'option',
            'passengercapacity' => 'option',
            'certified' => 'option',
            'sellingprice' => 'val',
            'miles' => 'val',
            'date' => 'option',
            'sortBy' => 'order',
            'search' => 'txt',
            'zip' => 'dealer',
            'dateinstock' => 'date',
            'dealer_id' => 'option',
            'min_price'   => 'min_price',
            'max_price'   => 'max_price',
            'min_year'   => 'year',
            'max_year'   => 'year',
            'search_make' => '',
            'search_model' => '',
            'search_zip' => '',
            'search_distance' => '',
            'search_type' => '',
            'search_body' => '',
            'dealrating' => 'rating',
            'features' => 'option',
            'search_price'   => 'price'
        ];

        if (!empty($querystring['search_zip'])) {
            $product = Product::with(['images', 'dealer'])->has('images');
        } else {
            // $product = Product::remember(1440);
            $product = Product::select('*');
        }

        $result = $product;
        $paginate = $this->pagination;
        $page = $request->get('page', 1);
        $offSet = ($page * $paginate) - $paginate;
        $max_distance = $querystring['search_distance'] ?? 100;

        foreach ($querystring as $key => $val) {

            if (empty($val)) continue;

            if ($key == $for) continue;

            if (array_key_exists($key, $type)) {
                if ($type[$key] === 'option') {
                    $val = explode(',', $val);
                    $val = array_filter($val, function ($value) {
                        return !is_null($value) && $value !== '';
                    });
                    if ($key == 'features') {
                        $options = OptionType::select('id')->where(function ($q) use ($key, $val) {
                            $q->WhereIn('name', $val);
                        })->get()->filter(function ($option) {
                            return !empty($option->id);
                        })->values()->toArray();
                        $product_options = ProductOption::select('product_id')->whereIn('product_options_type_id', $options)->get()->filter(function ($option) {
                            return !empty($option->product_id);
                        })->values()->toArray();
                        $result = $result->where(function ($q) use ($key, $product_options) {
                            $q->WhereIn('id', $product_options);
                        });
                    } else {
                        $result = $result->where(function ($q) use ($key, $val) {
                            $q->WhereIn($key, $val);
                        });
                    }
                } elseif ($type[$key] === 'type') {
                    if ($val == 'certified') {
                        $result = $result->where('certified', 'TRUE')->where('type', 'Used');
                    } elseif ($val == 'all') {
                        $result = $result->where(function ($q) use ($val) {
                            $q->whereIn('type', ['Used', 'New'])->orwhere('certified', 'TRUE');
                        });
                    } else {
                        $result = $result->where($key, $val);
                    }
                } elseif ($type[$key] === 'date') {
                    $result = $result->where($key, '>=', $val);
                } elseif ($type[$key] === 'rating') {
                    $val = explode(',', $val);
                    $val = array_filter($val, function ($value) {
                        return !is_null($value) && $value !== '';
                    });
                    $result = $result->where(function ($q) use ($key, $val) {
                        foreach ($val as $v) {
                            if ($v == 'good deal') {
                                if ($val[0] == 'good deal') {
                                    $q = $q->whereRaw('sellingprice between below_price and average_price');
                                } else {
                                    $q = $q->orwhereRaw('sellingprice between below_price and average_price');
                                }
                            } elseif ($v == 'great deal') {
                                if ($val[0] == 'great deal') {
                                    $q = $q->whereRaw('sellingprice between 1 and below_price');
                                } else {
                                    $q = $q->orwhereRaw('sellingprice between 1 and below_price');
                                }
                            } elseif ($v == 'fair price') {
                                if ($val[0] == 'fair price') {
                                    $q = $q->whereRaw('sellingprice > above_price');
                                } else {
                                    $q = $q->orwhereRaw('sellingprice > above_price');
                                }
                            }
                        }
                    });
                } elseif ($type[$key] === 'val') {
                    $val = explode(',', $val);
                    if ($key == 'market_days') {
                        $result = $result->where(function ($q) use ($val) {
                            $future_date_range = [
                                Carbon::today()->addDays($val[0])->toDateString(),
                                Carbon::today()->addDays($val[1])->toDateString()
                            ];
                            $past_date_range = [
                                Carbon::today()->subDays($val[1])->toDateString(),
                                Carbon::today()->subDays($val[0])->toDateString()
                            ];

                            $q->whereBetween('dateinstock', $past_date_range)->orWhereBetween('dateinstock', $future_date_range);
                        });
                    } else {
                        $result = $result->where(function ($q) use ($key, $val) {
                            $q->where($key, '>=', $val[0])->where($key, '<=', $val[1]);
                        });
                    }
                } elseif ($type[$key] === 'order') {
                    if (empty($val)) {
                        $result = $result->orderBy('updated_at', $val);
                    } else {
                        $val = explode(',', $val);
                        $result = $result->orderBy($val[0], $val[1]);
                    }
                } elseif ($type[$key] === 'bool') {
                    if ($val == 'true') {
                        $result = $result->where('certified', 'TRUE');
                    } else {
                        $result = $result->where('certified', 'FALSE');
                    }
                } elseif ($type[$key] === 'txt') {
                    $result = $result->where('make', 'like', '%' . $val . '%')->where('model', 'like', '%' . $val . '%');
                } elseif ($type[$key] === 'price') {
                    $result = $result->whereBetween('sellingprice', [0, $val]);
                } elseif ($type[$key] === 'multiple option') {
                    $val = explode(',', $val);
                    $result = $result->where(function ($q) use ($key, $val) {
                        foreach ($val as $v) {
                            $fields = explode('-', $v);

                            if ($key === 'engine') {
                                $q->orWhere('enginecylinders', $fields[0])->orWhere('enginedisplacement', $fields[1]);
                            }
                        }
                    });
                } elseif ($type[$key] === 'year') {
                    continue;
                } elseif ($type[$key] == 'min_price' || $key == 'max_price') {
                    continue;
                } else {
                    $key = substr($key, 0 + strlen('search_'), strlen($key));
                    if ($key == 'distance' || $key == 'zip' || $key == 'price') {
                        continue;
                    } else {
                        $result = $result->where($key, $val);
                    }
                }
            }
        }

        if (!empty($querystring['max_price']) && !empty($querystring['min_price'])) {
            $result = $result->whereBetween('sellingprice', [$querystring['min_price'], $querystring['max_price']]);
        } elseif (empty($querystring['max_price']) && !empty($querystring['min_price'])) {
            $result = $result->where('sellingprice', '>=', $querystring['min_price']);
        } elseif (!empty($querystring['max_price']) && empty($querystring['min_price'])) {
            $result = $result->whereBetween('sellingprice', [0, $querystring['max_price']]);
        }

        if (!empty($querystring['max_year']) && !empty($querystring['min_year'])) {
            $result = $result->whereBetween('year', [$querystring['min_year'], $querystring['max_year']]);
        } elseif (empty($querystring['max_year']) && !empty($querystring['min_year'])) {
            $result = $result->where('year', '>=', $querystring['min_year']);
        } elseif (!empty($querystring['max_year']) && empty($querystring['min_year'])) {
            $result = $result->whereBetween('year', [0, $querystring['max_year']]);
        }

        if (!empty($querystring['stm_lat']) && !empty($querystring['stm_lng'])) {
            $position["lat"] = $querystring['stm_lat'];
            $position["lng"] = $querystring['stm_lng'];
            $result = $result->selectRaw(DB::raw('*, round(st_distance_sphere(geo, Point('.$querystring['stm_lng'].', '.$querystring["stm_lat"].'))*0.000621371192,3)  as distance'))
            // ->whereRaw('st_distance_sphere(geo, Point(?, ?))*0.000621371192  < ?',[$querystring['stm_lng'], $querystring["stm_lat"], $max_distance])
            ->whereRaw('ST_Contains(ST_MakeEnvelope(Point(? + ? / 68.97237376812855 ,? + ? / 68.97237376812855),Point(? - ? / 68.97237376812855, ? - ? / 68.97237376812855)),geo)',[$position['lng'], $max_distance,  $position["lat"], $max_distance, $position['lng'], $max_distance,  $position["lat"], $max_distance]);
        } else if (!empty($querystring['search_zip'])) {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($querystring['search_zip'])."&sensor=false&key=".env('GOOGLE_MAPS_GEOCODING_API_KEY', null)."";
            $location_string = file_get_contents($url);
            $location = json_decode($location_string, true);
            if(!empty($location['results'])){
                $position = $location['results'][0]['geometry']['location'];
            }

            // $position = \Geocoder::getCoordinatesForAddress($querystring['search_zip']);

            if (empty($for)) {
                $result = $result->selectRaw(DB::raw('*, st_distance_sphere(POINT(longitude, latitude), Point('.$position['lng'].', '.$position["lat"].'))*0.000621371192  as distance'))
                             //->whereRaw('st_distance_sphere(geo, Point(?, ?))*0.000621371192  < ?',[$position['lng'], $position["lat"], $max_distance])
                             ->whereRaw('ST_Contains(ST_MakeEnvelope(Point(? + ? / 68.97237376812855 ,? + ? / 68.97237376812855),Point(? - ? / 68.97237376812855, ? - ? / 68.97237376812855)),POINT(longitude, latitude))',[$position['lng'], $max_distance,  $position["lat"], $max_distance, $position['lng'], $max_distance,  $position["lat"], $max_distance])
                             ->orderBy('distance', 'ASC');
            }
        }

        return $result;
    }

    /*
	* Car Deatail Page
	*/

    public function getCarDetail($vin)
    {
        if (empty($vin)) {
            return false;
        }
        $product = Product::with('ProductOption.option', 'ProductOption.category', 'images', 'dealer')->where('vin', $vin)->first();

        $product->images = $product->images->filter(function ($image) {
            return !empty($image->image);
        })->values();

        $p = $product->toArray();
        $product_option = [];
        if (!empty($p['product_option']))
            $product_option = $p['product_option'];

        $new_product_options = [];
        foreach ($product_option as $options) {
            $new_product_options[$options['category']['type']][] = [
                'id' => $options['option']['id'],
                'name' => $options['option']['name']
            ];
        }

        $product->product_options = $new_product_options;

        return response()->json($product);
    }

    /*
	* Search Dealers [by name,by make,by distance and zip]
	*/
    public function searchDealer(Request $request)
    {
        $dealers = Dealer::where('is_featured', '!=', 1)->withCount('dealerproduct');
        $data = [];
        $make = Product::where('make', '!=', '')->pluck('make')->unique();
        $data['make'] = $make;
        if ($request->name) {
            $dealer_name = $request->name;
            $dealers = Dealer::where('is_featured', '!=', 1)->where('name', 'like', '%' . $dealer_name . '%');
        } elseif ($request->make) {
            $val = explode(',', $request->make);
            $key = 'make';
            $result = Product::where(function ($q) use ($key, $val) {
                $q->WhereIn($key, $val);
            });
            $product_make = $result->groupBy('dealer_id')->pluck('dealer_id');
            if (!empty($product_make)) {
                $dealers = Dealer::whereIn('dealer_id', $product_make);
            } else {
                $dealers = [];
            }
        } elseif ($request->location) {
            $distance = $request->distance;
            $zip = $request->zip;
            $dealers = Dealer::where('distance', '<=', $distance)->orWhere('zip', $zip);
        }
        $dealers = $dealers->paginate($this->pagination);

        $data['dealers'] = $dealers;

        return response()->json($data);
    }

    public function filterDealRatingRequest($request)
    {
        $client = new \GuzzleHttp\Client();
        $product = Product::select('*')->get();
        foreach ($product as $key => $value) {
            $response = $client->request('GET', 'http://marketvalue.vinaudit.com/getmarketvalue.php?key=RX0Y395X9YW8F79&vin=' . $value->vin);
            $result = json_decode($response->getBody());
            print_r($result);
        }
        die;
    }

    public function sendMessage(Request $request)
    {
        $emailParams = $request->all();
        $validator = Validator::make($emailParams, [
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([$validator]);
        }

        $email = new ContactMessageSent($emailParams);
        Mail::to('leads@cardesta.com')->send($email);

        return response()->json(['status' => true]);
    }

    public function migrateESDB(Request $request)
    {
        Product::createIndex($shards = null, $replicas = null);
        Product::chunk(500, function ($chunks) {
            foreach ($chunks as $chunk) {
                $chunk->addToIndex();
            }
            unset($chunks);
        });

        return response()->json(["Success" => "Success"]);
    }
}
