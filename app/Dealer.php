<?php

namespace App;

use Malhal\Geographical\Geographical;
use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Dealer extends Model
{
    use SoftDeletes,Geographical,Rememberable;

    protected $fillable = [
        'dealer_id',
        'name',
        'phone',
        'email',
        'address',
        'state',
        'city',
        'fax',
        'website',
        'product_import',
    ];
    protected $table = 'dealer';
    protected $dates = ['deleted_at'];

    protected $hidden=['created_at', 'updated_at','deleted_at','google_conversion_id', 'google_conversion_label','imported','product_id','active','id','dealer_id'];

    public function featuredproduct(){
        return $this->hasMany(Product::class,'dealer_id','dealer_id')->limit(8);
    }

    public function dealerproduct(){
        return $this->hasMany(Product::class,'dealer_id','dealer_id');
    }

}
